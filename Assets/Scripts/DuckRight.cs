﻿using UnityEngine;
using System.Collections;

public class DuckRight : MonoBehaviour {


    public bool isArrive;
    public GameObject target;
    int speed;
    public int minspd, maxspd;

    // Use this for initialization
    void Start()
    {
        speed = Random.Range(minspd, maxspd);
    }

    // Update is called once per frame
    void Update()
    {
        if (isArrive != true)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(target.transform.position.x, this.transform.position.y), speed * Time.deltaTime);
            if (transform.position.x == target.transform.position.x)
            {
                isArrive = true;
            }
        }
        if (isArrive == true)
        {
            Destroy(gameObject);
        }
    }
}
