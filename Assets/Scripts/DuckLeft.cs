﻿using UnityEngine;
using System.Collections;

public class DuckLeft : MonoBehaviour {

    public bool isArrive;
    public GameObject target;
    int speed;
    public int minspd, maxspd;

    //kkkkkk
    public int belok;
    public float duration_before_belok;
    public float time_before_belok;

    //end

	// Use this for initialization
	void Start () {
        speed = Random.Range(minspd, maxspd);
        time_before_belok = duration_before_belok;
        //transform.rotation.Set(0f, 0f, 90f, 0f);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (isArrive != true)
        {
            //ar
            time_before_belok -= Time.deltaTime;
            if (belok > 0)
            {
                if (time_before_belok <= 0)
                {
                    belok--;
                    time_before_belok = duration_before_belok;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z+15);
                 
                }

                transform.Translate(new Vector3(4f,0f,0f) * Time.deltaTime);
            }
            if (belok <= 0)
                //end ar
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(target.transform.position.x, this.transform.position.y), speed * Time.deltaTime);
            if (transform.position.x == target.transform.position.x)
            {
                isArrive = true;
            }
        }
        if (isArrive == true)
        {
            Destroy(gameObject);
        }
	}
}
