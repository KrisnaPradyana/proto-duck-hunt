﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	void Update() {
		transform.Translate(Vector3.forward * 20 * Time.deltaTime);
		if (transform.position.z > 100)
			Destroy (gameObject);
	}
}