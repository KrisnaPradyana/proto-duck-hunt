﻿using UnityEngine;
using System.Collections;
//32, 27
public class SpawnerBehaviour : MonoBehaviour {

    float interval;
    float dof;
    bool top;
    public float drangemin, drangemax;
    public float intervalmin, intervalmax;
    public GameObject[] duck;
    public int UpDownspeed;
	// Use this for initialization
	void Start () 
    {
	     interval = Random.Range(intervalmin, intervalmax);
	}
	
	// Update is called once per frame
	void Update () 
    {
        /*
        Debug.Log(top); 

        if (top != true)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(this.transform.position.x, 32), UpDownspeed * Time.deltaTime);
            top = true;
        }
        else
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(this.transform.position.x, 3), UpDownspeed * Time.deltaTime);
            top = false;
           */
        interval = interval - Time.deltaTime;
        if (interval <= 0)
        {
            spawn();
        }
	}

    void spawn()
    {
        int randomduck = Random.Range(0, duck.Length);
        Instantiate(duck[randomduck], this.transform.position, duck[randomduck].transform.rotation);
      
        interval = Random.Range(intervalmin, intervalmax);
    }
}

