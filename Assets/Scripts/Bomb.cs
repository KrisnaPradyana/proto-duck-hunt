﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {

	void Update() {
		transform.Translate(Vector3.forward * 10 * Time.deltaTime);
		if (transform.position.z > 100)
			Destroy (gameObject);
	}
}
