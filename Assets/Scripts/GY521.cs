﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class GY521 : MonoBehaviour
{
    public SerialPort serial = new SerialPort("COM3", 19200);
    public GameObject bullet, bomb;

    string imuString;
    string[] imuArray;
    float rotX, rotY, rotZ, tempX, tempY, tempZ, delX, delY, delZ;
    int aState, rState, hold;
    bool reset, start;

    void Start()
    {
        if (serial.IsOpen == false)
            serial.Open();
    }

    void Update()
    {

        imuString = serial.ReadLine();
        imuArray = imuString.Split(" "[0]);

        rotX = -1 * float.Parse(imuArray[0]);
        rotY = -1 * float.Parse(imuArray[2]);
        rotZ = -1 * float.Parse(imuArray[1]);

        aState = int.Parse(imuArray[3]);
        rState = int.Parse(imuArray[4]);

        if (!start)
        {
            tempX = rotX;
            tempY = rotY;
            tempZ = rotZ;
            start = true;
        }

        if (aState != 1)
        {
            if (hold < 10 && hold > 0)
                Instantiate(bullet, transform.position, transform.rotation);
            else if (hold >= 10)
                Instantiate(bomb, transform.position, transform.rotation);
            hold = 0;
        }
        else
            hold += 1;

        if (rState == 1 && !reset)
        {
            tempX = rotX;
            tempY = rotY;
            tempZ = rotZ;
            reset = true;
            start = true;
        }
        else
            reset = false;

        delX = rotX - tempX;
        delY = rotY - tempY;
        delZ = rotZ - tempZ;

        Debug.Log("x = " + rotX + " | y = " + rotY + " | z = " + rotZ + " | a = " + hold);
        transform.rotation = Quaternion.Euler(delX, delY, delZ);
    }
}