﻿using UnityEngine;
using System.Collections;

public class GunControl : MonoBehaviour {

    public int aimSensitify;
    public float aimSpeed;
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
 ///////////////////////////////////////////////////////////////////Rotate Horizontally and Verticaly//////////////////////////////////////////////////////////////////////////
	  if (Input.GetKey(KeyCode.W))
      {
          transform.Rotate(new Vector3(aimSensitify, 0, 0)*Time.deltaTime);
      }
      if (Input.GetKey(KeyCode.S))
      {
          transform.Rotate(new Vector3(-aimSensitify, 0, 0)* Time.deltaTime);
      }
      if (Input.GetKey(KeyCode.D))
      {
          transform.Rotate(new Vector3(0, aimSensitify, 0) * Time.deltaTime);
      }
      if (Input.GetKey(KeyCode.A))
      {
          transform.Rotate(new Vector3(0, -aimSensitify, 0) * Time.deltaTime);
      }
//////////////////////////////////////////////////////////////////////Strafe Horizontally and Verticaly///////////////////////////////////////////////////////////////////////
      if (Input.GetKey(KeyCode.UpArrow))
      {
          transform.Translate(0, aimSpeed * Time.deltaTime, 0, Space.World);
      }
      if (Input.GetKey(KeyCode.DownArrow))
      {
          transform.Translate(0, -aimSpeed * Time.deltaTime, 0, Space.World);
      }
      if (Input.GetKey(KeyCode.LeftArrow))
      {
           transform.Translate(-aimSpeed * Time.deltaTime, 0, 0, Space.World);
      }
      if (Input.GetKey(KeyCode.RightArrow))
      {
          transform.Translate(aimSpeed * Time.deltaTime, 0, 0, Space.World);
      }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
}
